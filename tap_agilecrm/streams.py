"""Stream type classes for tap-agilecrm."""

from singer_sdk import typing as th

from tap_agilecrm.client import AgileCrmStream
from tap_agilecrm.client_dynamic_filter import DynamicFilterStream


class ContactsSearch(DynamicFilterStream):
    name = "contacts"
    path = "/filters/filter/dynamic-filter"
    primary_keys = ["id"]
    replication_key = "updated_time"
    contact_type = "PERSON"

    default_schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("type", th.StringType),
        th.Property("created_time", th.DateTimeType),
        th.Property("updated_time", th.DateTimeType),
        th.Property("last_contacted", th.DateTimeType),
        th.Property("last_emailed", th.DateTimeType),
        th.Property("last_campaign_emailed", th.DateTimeType),
        th.Property("last_called", th.DateTimeType),
        th.Property("viewed_time", th.DateTimeType),
        th.Property("viewed", th.ObjectType(
            th.Property("viewed_time", th.DateTimeType)
        )),
        th.Property("star_value", th.IntegerType),
        th.Property("lead_score", th.IntegerType),
        th.Property("klout_score", th.StringType),
        th.Property("tags", th.ArrayType(th.StringType)),
        th.Property("tagsWithTime", th.ArrayType(th.ObjectType(
            th.Property("tag", th.StringType),
            th.Property("createdTime", th.DateTimeType),
            th.Property("availableCount", th.IntegerType),
            th.Property("entity_type", th.StringType)
        ))),
        th.Property("properties", th.ArrayType(th.ObjectType(
            th.Property("type", th.StringType),
            th.Property("name", th.StringType),
            th.Property("value", th.StringType),
            th.Property("subtype", th.StringType)
        ))),
        th.Property("address_zip", th.StringType),
        th.Property("address_country", th.StringType),
        th.Property("address_address", th.StringType),
        th.Property("address_city", th.StringType),
        th.Property("address_countryname", th.StringType),
        th.Property("address_state", th.StringType),
        th.Property("campaignStatus", th.ArrayType(th.StringType)),
        th.Property("entity_type", th.StringType),
        th.Property("contact_company_id", th.StringType),
        th.Property("unsubscribeStatus", th.ArrayType(th.StringType)),
        th.Property("emailBounceStatus", th.ArrayType(th.StringType)),
        th.Property("formId", th.IntegerType),
        th.Property("browserId", th.ArrayType(th.StringType)),
        th.Property("lead_source_id", th.IntegerType),
        th.Property("lead_status_id", th.IntegerType),
        th.Property("is_lead_converted", th.BooleanType),
        th.Property("lead_converted_time", th.DateTimeType),
        th.Property("is_duplicate_existed", th.BooleanType),
        th.Property("trashed_time", th.DateTimeType),
        th.Property("restored_time", th.DateTimeType),
        th.Property("is_duplicate_verification_failed", th.BooleanType),
        th.Property("is_client_import", th.BooleanType),
        th.Property("concurrent_save_allowed", th.BooleanType),
        th.Property("owner", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("domain", th.StringType),
            th.Property("email", th.StringType),
            th.Property("phone", th.StringType),
            th.Property("name", th.StringType),
            th.Property("pic", th.StringType),
            th.Property("schedule_id", th.StringType),
            th.Property("calendar_url", th.StringType),
            th.Property("calendarURL", th.StringType)
        ))
    ).to_dict()


class DealsSearch(AgileCrmStream):
    name = "deals"
    path = "/opportunity"
    primary_keys = ["id"]
    
    default_schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("contacts", th.CustomType({"type": ["array", "string"]})),
        th.Property("description", th.StringType),
        th.Property("close_date", th.DateTimeType),
        th.Property("created_time", th.DateTimeType),
        th.Property("expected_value", th.NumberType),
        th.Property("milestone", th.StringType),
        th.Property("entity_type", th.StringType),
        th.Property("probability", th.NumberType),
        th.Property("owner", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("email", th.StringType),
            th.Property("is_admin", th.BooleanType),
            th.Property("is_account_owner", th.BooleanType),
            th.Property("is_disabled", th.BooleanType),
        )),
        th.Property("prefs", th.CustomType({"type": ["object", "string"]})),
        th.Property("pic", th.StringType),
        
    ).to_dict()


class CompaniesSearch(ContactsSearch):
    name = "companies"
    path = "/contacts/companies/list"
    primary_keys = ["id"]
    contact_type = "COMPANY"
    #Needs to be tested
    replication_key = "updated_time"
