"""AgileCrm tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_agilecrm.streams import (
    AgileCrmStream,
    CompaniesSearch,
    ContactsSearch,
    DealsSearch,
)

STREAM_TYPES = [ContactsSearch, DealsSearch, CompaniesSearch]


class TapAgileCrm(Tap):
    """AgileCrm tap class."""

    name = "tap-agilecrm"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "username",
            th.StringType,
            required=True,
            description="The username to authenticate against the API service",
        ),
        th.Property(
            "password",
            th.StringType,
            required=True,
            description="The password (REST API Key) to authenticate against the API service using Basic Authentication",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property(
            "account",
            th.StringType,
            required=True,
            description="The account to compose url for the API service",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapAgileCrm.cli()
