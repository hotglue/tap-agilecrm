"""REST client handling, including AgileCrmStream base class."""
import json
from datetime import datetime
from pathlib import Path
from typing import Any, Dict, Iterable, List, Optional, Union

import requests
from backports.cached_property import cached_property
from memoization import cached
from pendulum import parse
from singer_sdk import typing as th
from singer_sdk.authenticators import BasicAuthenticator
from singer_sdk.streams import RESTStream

from tap_agilecrm.client import AgileCrmStream


class DynamicFilterStream(AgileCrmStream):
    """AgileCrm stream class."""

    rest_method = "POST"
    _page_size = 500

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        """Prepare the data payload for the REST API request.

        By default, no payload will be sent (return None).
        """
        start_date = self.get_starting_time(context)
        utc_time = None
        if start_date:
            utc_time = int(start_date.timestamp() * 1000)

        filter_dict = {}
        if self.replication_key:
            filter_dict = {
                "rules": [],
                "or_rules": [
                    {"LHS": "created_time", "CONDITION": "AFTER", "RHS": utc_time},
                    {"LHS": "updated_time", "CONDITION": "AFTER", "RHS": utc_time},
                ],
                "contact_type": self.contact_type,
            }

        payload = {
            "radius": 10,
            "page_size": self._page_size,
            "global_sort_key": "-created_time",
            "filterJson": json.dumps(filter_dict),
        }
        if next_page_token:
            payload["cursor"] = next_page_token
        return payload

    def get_url_params(self, context, next_page_token) -> Dict[str, Any]:
        return {}
