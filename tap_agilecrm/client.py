"""REST client handling, including AgileCrmStream base class."""
import json
import time
from datetime import datetime, timezone
from pathlib import Path
from typing import Any, Dict, Iterable, List, Optional, Union

import requests
from backports.cached_property import cached_property
from memoization import cached
from pendulum import parse
from singer_sdk import typing as th
from singer_sdk.authenticators import BasicAuthenticator
from singer_sdk.streams import RESTStream


class AgileCrmStream(RESTStream):
    """AgileCrm stream class."""

    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        return f"https://{self.config.get('account')}.agilecrm.com/dev/api"

    _page_size = 500

    records_jsonpath = "$[*]"
    next_page_token_jsonpath = "$.next_page"

    @property
    def authenticator(self) -> BasicAuthenticator:
        """Return a new authenticator object."""
        return BasicAuthenticator.create_for_stream(
            self,
            username=self.config.get("username"),
            password=self.config.get("password"),
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        headers["Accept"] = "application/json"
        return headers

    def prepare_request(self, context, next_page_token) -> requests.PreparedRequest:
        """Prepare a request object for this stream."""
        http_method = self.rest_method
        url: str = self.get_url(context)
        params: dict = self.get_url_params(context, next_page_token)
        request_data = self.prepare_request_payload(context, next_page_token)
        headers = self.http_headers

        return self.build_prepared_request(
            method=http_method,
            url=url,
            params=params,
            headers=headers,
            data=request_data,
        )

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        r_json = response.json()
        if r_json:
            next_page_token = r_json[-1].get("cursor", None)
            return next_page_token
        else:
            return None

    @cached
    def get_starting_time(self, context):
        start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date

    def get_datetime_keys(self, schema):
        output = {}
        for key, value in schema["properties"].items():
            if value.get("format") == "date-time":
                output[key] = "date-time"
            elif (
                "array" in value.get("type")
                and "items" in value
                and "object" in value["items"]["type"]
                and value["items"].get("properties")
            ):
                dt_items = self.get_datetime_keys(value["items"])
                if dt_items:
                    output[key] = [dt_items]
            elif "object" in value.get("type") and value.get("properties"):
                dt_items = self.get_datetime_keys(value)
                if dt_items:
                    output[key] = dt_items
        return output

    @property
    @cached
    def datetime_fields(self):
        return self.get_datetime_keys(self.schema)

    def to_datetime(self, datetime_field, row):
        for field in datetime_field:
            if isinstance(row.get(field), dict):
                row[field] = self.to_datetime(datetime_field[field], row[field])
            elif isinstance(row.get(field), list):
                row[field] = [
                    self.to_datetime(datetime_field[field][0], f) for f in row[field]
                ]
            elif row.get(field):
                timestamp = row[field]

                # If the timestamp looks like it's in milliseconds, convert to seconds
                if timestamp > 1e10:  # Larger than typical Unix timestamp in seconds
                    timestamp /= 1000

                row[field] = (
                    datetime.utcfromtimestamp(timestamp)
                    .replace(tzinfo=None)
                    .isoformat()
                )
            else:
                row[field] = None
        return row

    def get_sub_properties(self, row: dict):
        sub_properties = dict()
        properties = row.get("properties")
        if properties and type(properties):
            for sub_property in properties:
                if sub_property["name"] == "address":
                    try:
                        address_sub_properties = json.loads(sub_property["value"])
                        for address_sub_property in address_sub_properties:
                            sub_properties["address_"+address_sub_property] = address_sub_properties[address_sub_property]
                    except Exception as e:
                        continue                        
                else:
                    sub_properties[sub_property["name"]] = sub_property["value"]
        return sub_properties

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        row.pop("cursor",{})
        if self.replication_key:
            if self.replication_key == "updated_time":
                if not row.get("updated_time"):
                    row["updated_time"] = row["created_time"]

        row = self.to_datetime(self.datetime_fields, row)
        row.update(self.get_sub_properties(row))
        return row

    def get_url_params(self, context, next_page_token) -> Dict[str, Any]:
        params = {}
        params["page_size"] = self._page_size
        if next_page_token:
            params["cursor"] = next_page_token
        return params

    def is_unix_timestamp(self, number):
        try:
            converted_date = datetime.utcfromtimestamp(number)
            if converted_date.year > 1970:
                return True
            else:
                return False
        except:
            return False

    def get_jsonschema_type(self, obj):
        dtype = type(obj)

        if dtype == int:
            return th.IntegerType()
        if dtype == float:
            return th.NumberType()
        if dtype == str:
            return th.StringType()
        if dtype == bool:
            return th.BooleanType()
        if dtype == list:
            if len(obj) > 0:
                return th.ArrayType(self.get_jsonschema_type(obj[0]))
            else:
                return th.ArrayType(
                    th.CustomType({"type": ["number", "string", "object"]})
                )
        if dtype == dict:
            obj_props = []
            for key in obj.keys():
                obj_props.append(th.Property(key, self.get_jsonschema_type(obj[key])))
            return th.ObjectType(*obj_props)

        raise ValueError(f"Unmappable data type '{dtype}'.")

    def get_schema(self) -> dict:
        """Dynamically detect the json schema for the stream.
        This is evaluated prior to any records being retrieved.
        """

        self._requests_session = requests.Session()
        # Get the data
        headers = self.http_headers
        headers.update(self.authenticator._auth_headers)
        path = self.path

        request_payload = self.prepare_request_payload({},None)
        if request_payload:
            if "filterJson" in request_payload:
                #remove replication logic
                if isinstance(request_payload['filterJson'],str):
                    request_payload['filterJson'] = json.loads(request_payload['filterJson'])
                request_payload['filterJson'].update({"or_rules":[]})   
        request_type = self.rest_method
        if self.name=="contacts":
            path = "/contacts"   
            request_payload = None
            request_type = "GET"
        url = self.url_base + path
        response = requests.request(
            request_type,
            url,
            headers=headers,
            params={"page_size": 1} if request_type=="GET" else {},
            json=request_payload
        )
        records = response.json()
        if len(records)>0:
            properties = []
            property_names = set()

            # Loop through all records – some objects have different keys
            for record in records:
                # Loop through each key in the object
                for name in record.keys():
                    if name in property_names or name == "cursor":
                        continue
                    # Add the new property to our list
                    sub_properties = []
                    if name == "properties":
                        for property in record[name]:
                            if property['name'] == 'address':
                                try:
                                    address_fields = json.loads(property["value"])
                                    for address_field in address_fields:
                                        sub_properties.append({"name": 'address_'+address_field, "value":address_fields[address_field]})
                                except Exception as e:
                                    continue
                            else:
                                sub_properties.append(property)
                    else:
                        sub_properties.append({"name": name, "value":record[name]})
                    
                    for sub_property in sub_properties:
                        property_names.add(sub_property["name"])
                        if self.is_unix_timestamp(sub_property["value"]) or "_time" in sub_property["name"]:
                            properties.append(th.Property(sub_property["name"], th.DateTimeType))
                        else:
                            properties.append(
                                th.Property(sub_property["name"], self.get_jsonschema_type(sub_property["value"]))
                            )
                #we need to process only first record        
                break        
            # Return the list as a JSON Schema dictionary object
            property_list = th.PropertiesList(*properties).to_dict()
            self.default_schema['properties'].pop('properties',{})
            property_list.update(self.default_schema)

            return property_list
        else:
            return self.default_schema

    @cached_property
    def schema(self) -> dict:
        return self.get_schema()
